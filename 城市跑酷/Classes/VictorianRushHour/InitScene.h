#ifndef _INIT_SECENE_H_
#define _INIT_SECENE_H_

#include "cocos2d.h"
USING_NS_CC;

class InitScene : public cocos2d::CCLayer
{
public:
	InitScene();
	//初始化是框架里每个Node所共有的  用virtual关键字修饰
	virtual bool init();

	static cocos2d::CCScene* scene();

	void waitRedirect(float delay);

	CREATE_FUNC(InitScene);
protected:
private:
	CCSize _size;
};


#endif	