#include "InitScene.h"
#include "VisibleRect.h"
#include "GameLayer.h"
USING_NS_CC;

using namespace std;

InitScene::InitScene(){}

CCScene* InitScene::scene(){
	CCScene* scene = CCScene::create();
	InitScene* layer = InitScene::create();
	scene->addChild(layer);
	return scene;
}

bool InitScene::init(){
	_size = CCDirector::sharedDirector()->getWinSize();
	CCLabelTTF* labelTTF = CCLabelTTF::create("AMaos Little Game","Arial",44);
	labelTTF->setPosition(ccp(VisibleRect::center().x,VisibleRect::center().y+70));
	this->addChild(labelTTF);
	//����
	CCMoveTo* actionLeft = CCMoveTo::create(2.0f,ccp(VisibleRect::center().x-50,VisibleRect::center().y+70));
	//����
	CCMoveTo* actionRight = CCMoveTo::create(2.0f,ccp(VisibleRect::center().x+50,VisibleRect::center().y+70));
	labelTTF->runAction(CCSequence::create(actionLeft,actionRight,NULL));
	
	CCLabelTTF* initTTF = CCLabelTTF::create("Resources are loading, immediately into the game world","Arial",44);
	initTTF->setPosition(ccp(_size.width/2,_size.height/2));
	initTTF->setAnchorPoint(ccp(0.5,0.5));
	this->addChild(initTTF);


	this->scheduleOnce(schedule_selector(InitScene::waitRedirect),3);

	return true;
}

void InitScene::waitRedirect(float delay){

	CCScene* scene = GameLayer::scene();
	CCDirector::sharedDirector()->replaceScene(scene);

}
